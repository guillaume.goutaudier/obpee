# Oracle Blockchain Platform Enterprise Edition
This repository is a simplified tutorial explaining how to easily deploy and use the Oracle Blockchain Platform Enterprise Edition (OBPEE) on the Oracle Cloud Infrastructure (OCI).

# Prepare OBPEE Virtual Machine
### Get OBPEE official image
Download the latest version of OBPEE from https://edelivery.oracle.com/osdc/faces/SoftwareDelivery
and upload the image on Object Storage.

Then create a pre-authenticated link and use it to create a custom image.

Then create a virtual machine with at least 2 OCPUs from this custom image.

### Console access
Access the newly created VM using the console:
- User: oracle
- Password: Welcome1

You will be asked to change the password. 

### Network interface configuration
Edit `/etc/sysconfig/network-scripts/ifcfg-enp0s3` and replace `DEVICE` by `eth0`

### Public DNS
Note the public IP address of the VM, and create a public DNS entry for it, e.g. wisekeydemo.swissledger.io

### Update hostname
Set hostname to the public DNS, e.g. wisekeydemo.swissledger.io:
```
hostnamectl set-hostname wisekeydemo.swissledger.io
```

### Validate SSH connection
Reboot the VM and validate you can connect via SSH using the public DNS name.


### [Optional] Operating System fine tuning
```
# To get rid of the LC_CTYPE warning
[oracle@obpee ~]$ echo -e "LANG=en_US.utf-8\nLC_ALL=en_US.utf-8" | sudo tee -a /etc/environment
[oracle@obpee ~]$ echo "LC_CTYPE=en_US.utf-8" | sudo tee -a /etc/locale.conf
# To connect with SSH key
[oracle@obpee ~]$ mkdir .ssh
[oracle@obpee ~]$ chmod 700 .ssh
[oracle@obpee ~]$ vi .ssh/authorized_keys
# Copy your SSH public key
[oracle@obpee ~]$ chmod 600 .ssh/authorized_keys
```



### LDAP configuration
Connect to https://wisekeydemo.swissledger.io:7443/console/index.html using the following credentials (use Firefox):
- User: obpadmin
- Password: welcome1

Set LDAP password to `Welcome1`
Click "Save and Set Active"

### New user creation
```
cd /u01/blockchain/ldap/environment/
./adduser.sh obpuser <Plaftorm Manager Name, e.g. BPM5987>
obpuser password: Welcome1
Admin password: Welcome1
```

Sign out and validate that you can re-connect to https://wisekeydemo.swissledger.io:7443/console/index.html using the ubpuser credentials

# Blockchain Instance creation
### Create instance
Connect to https://wisekeydemo.swissledger.io:7443/console/index.html using the ubpuser credentials

Create a new instance:
- Use public DNS name in Cluster section

The instance creation will take up to 10min.

### Interact with the instance
Connect to the service console with obpuser:
https://wisekeydemo.swissledger.io:10000/

Deploy the `sacc` chaincode.

Use the scripts from the demo folder to interact with the chaincode.

# [Optional] Create Boot Volume Backup
It might be a good idea to save all what we have done by creating a backup of your VM. Go to `Compute / Boot Volumes`, select the boot volume that corresponds to your instance and select `Create Manual Backup`.




